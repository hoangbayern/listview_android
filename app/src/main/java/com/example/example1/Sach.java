package com.example.example1;

public class Sach {
    private String TieuDe;
    private String TacGia;
    private String SoTrang;

    public Sach(String tieuDe, String tacGia, String soTrang) {
        TieuDe = tieuDe;
        TacGia = tacGia;
        SoTrang = soTrang;
    }

    public String getTieuDe() {
        return TieuDe;
    }

    public void setTieuDe(String tieuDe) {
        TieuDe = tieuDe;
    }

    public String getTacGia() {
        return TacGia;
    }

    public void setTacGia(String tacGia) {
        TacGia = tacGia;
    }

    public String getSoTrang() {
        return SoTrang;
    }

    public void setSoTrang(String soTrang) {
        SoTrang = soTrang;
    }
}

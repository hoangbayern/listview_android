package com.example.example1;

public class SanPham {
    private  String maSp;
    private String tenSp;
    private String soluongSp;

    public SanPham(String maSp, String tenSp, String soluongSp) {
        this.maSp = maSp;
        this.tenSp = tenSp;
        this.soluongSp = soluongSp;
    }

    @Override
    public String toString() {
        return
                "maSp= " + maSp + '-' +
                ", tenSp= " + tenSp + '-' +
                ", soluongSp= " + soluongSp;
    }

    public SanPham() {
    }
    public String getMaSp() {
        return maSp;
    }

    public void setMaSp(String maSp) {
        this.maSp = maSp;
    }

    public String getTenSp() {
        return tenSp;
    }

    public void setTenSp(String tenSp) {
        this.tenSp = tenSp;
    }

    public String getSoluongSp() {
        return soluongSp;
    }

    public void setSoluongSp(String soluongSp) {
        this.soluongSp = soluongSp;
    }



}

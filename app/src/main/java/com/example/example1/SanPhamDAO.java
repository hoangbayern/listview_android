package com.example.example1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SanPhamDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context context;

    public SanPhamDAO(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);//Thuc thi tao database
        db = dbHelper.getWritableDatabase();//Cho phep ghi du lieu vao database
    }
    //Chèn mới
    public int insertProduct(SanPham product) {
        ContentValues values = new ContentValues();

//        values.put("maSp", product.getMaSp());
        values.put("tenSp", product.getTenSp());
        values.put("slSP", product.getSoluongSp());
        //Insert vao database
        long kq = db.insert("product", null, values);
        //Kiem tra
        if (kq<=0){
            return -1; //Insert that bai
        }
        return 1; //Insert thanh cong

    }
    //Hien thi du lieu
    public List<SanPham> getAllProducts() {

        ArrayList<SanPham> products = new ArrayList<>();
        int sum = 0;

        Cursor cursor = db.rawQuery("SELECT maSp, tenSp, slSP from product ORDER BY maSp DESC", null);

        //Đến dòng đầu của tập dữ liệu
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SanPham sp = new SanPham();
            sp.setMaSp(cursor.getString(0));
            sp.setTenSp(cursor.getString(1));
            sp.setSoluongSp(cursor.getString(2));

            try {
                int TongSL = Integer.parseInt(cursor.getString(2));
                sum += TongSL;
            }catch (Exception e){

            }
            products.add(sp);
            cursor.moveToNext();
        }


        cursor.close();
        Toast.makeText(context, "Tong SL: "+sum, Toast.LENGTH_SHORT).show();
        return products;
    }
    //Xoa Product
    public int deleteProduct(String maSp){
        //Xoa
        int kq = db.delete("product", "maSp=?", new String[]{maSp});
        //Kiem tra
        if (kq<=0){
            return -1;
        }
        return 1;
    }
    //Update Product
    public int updateProduct(SanPham product) {
        ContentValues values = new ContentValues();

//        values.put("maSp", product.getMaSp());
        values.put("tenSp", product.getTenSp());
        values.put("slSP", product.getSoluongSp());
        //Update vao database
        long kq = db.update("product", values, "maSp=?", new String[]{product.getMaSp()});
        //Kiem tra
        if (kq<=0){
            return -1; //Update that bai
        }
        return 1; //Update thanh cong

    }

}

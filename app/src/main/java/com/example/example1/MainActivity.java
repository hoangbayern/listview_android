package com.example.example1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnThem, btnSua, btnXoa, btnShow;
    ListView listView;
    ProductAdapter arrayAdapter;
    SanPhamDAO sanPhamDAO;
    List<SanPham> list = new ArrayList<>();
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Anh xa
        AnhXa();

        //Khoi tao cac bien
        context = this;
        //Hien thi du lieu khi chay chuong trinh
        list.clear();
        sanPhamDAO = new SanPhamDAO(context);
        list = sanPhamDAO.getAllProducts();
        arrayAdapter = new ProductAdapter(context, R.layout.item_list, list);

        listView.setAdapter(arrayAdapter);
        //Button hien thi
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                sanPhamDAO = new SanPhamDAO(context);
                list = sanPhamDAO.getAllProducts();
                arrayAdapter = new ProductAdapter(context, R.layout.item_list, list);
                listView.setAdapter(arrayAdapter);
            }
        });
        //Button Them
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAdd();
            }
        });
        //
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                showDialogEdit(position);
            }
        });
    }

    private void AnhXa() {
        btnThem = findViewById(R.id.btnThem);
        btnSua = findViewById(R.id.btnSua);
        btnXoa = findViewById(R.id.btnXoa);
        btnShow = findViewById(R.id.btnShow);
        listView = findViewById(R.id.lstView);
    }
    private void showDialogAdd(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.them_sanpham, null);
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.show();

       EditText txtMa = view.findViewById(R.id.txt1);
       EditText txtTen = view.findViewById(R.id.txt2);
       EditText txtSL = view.findViewById(R.id.txt3);
       Button btnAdd = view.findViewById(R.id.btnAdd);

       btnAdd.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               SanPham sp = new SanPham();
                //Dua data va Doi tuong
                sp.setMaSp(txtMa.getText().toString());
                sp.setTenSp(txtTen.getText().toString());
                sp.setSoluongSp(txtSL.getText().toString());
                //Goi ham insert Product
                int kq = sanPhamDAO.insertProduct(sp);
                //Kiem tra insert thanh cong hay khong
                if (kq==-1){
                    Toast.makeText(context, "Insert that bai", Toast.LENGTH_LONG).show();
                }
                if (kq==1){
                    Toast.makeText(context, "Insert thanh cong", Toast.LENGTH_LONG).show();
                    list.clear();
                    list.addAll(sanPhamDAO.getAllProducts());
                    arrayAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
           }
       });

    }
    private void showDialogEdit(int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.sua_sanpham, null);
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.show();
        //Anh xa
        EditText txtMa = view.findViewById(R.id.txtEdit1);
        EditText txtTen = view.findViewById(R.id.txtEdit2);
        EditText txtSL = view.findViewById(R.id.txtEdit3);
        Button btnUpdate = view.findViewById(R.id.btnUpdate);
        Button btnDelete = view.findViewById(R.id.btnDelete);

        //Fill data vao text de Update
        SanPham product = list.get(position);
        txtTen.setText(product.getTenSp());
        txtSL.setText(product.getSoluongSp());

        //Update Product
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Dua data va Doi tuong
                product.setTenSp(txtTen.getText().toString());
                product.setSoluongSp(txtSL.getText().toString());
                //Goi ham update Product
                int kq = sanPhamDAO.updateProduct(product);
                //Kiem tra insert thanh cong hay khong
                if (kq==-1){
                    Toast.makeText(context, "Update that bai", Toast.LENGTH_LONG).show();
                }
                if (kq==1){
                    Toast.makeText(context, "Update thanh cong", Toast.LENGTH_LONG).show();
                    list.clear();
                    list.addAll(sanPhamDAO.getAllProducts());
                    arrayAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        });
        //Xoa San Pham
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Goi ham delete Product
                int kq = sanPhamDAO.deleteProduct(product.getMaSp());
                //Kiem tra insert thanh cong hay khong
                if (kq==-1){
                    Toast.makeText(context, "Delete that bai", Toast.LENGTH_LONG).show();
                }
                if (kq==1){
                    Toast.makeText(context, "Delete thanh cong", Toast.LENGTH_LONG).show();
                    list.clear();
                    list.addAll(sanPhamDAO.getAllProducts());
                    arrayAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        });

    }
}
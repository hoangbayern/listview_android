package com.example.example1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "ProductDbHelper";
    private static final String DATABASE_NAME = "myproduct.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_PRODUCT = "product";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Phương thức này tự động gọi nếu storage chưa có DATABASE_NAME
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_PRODUCT + " ( " +
                "maSp INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "tenSp VARCHAR (255) NOT NULL, " +
                "slSP VARCHAR (255) NOT NULL" +
                ")";
        db.execSQL(sql);
        sql = "INSERT INTO product VALUES (null, 'Dien Thoai', 200000)";
        db.execSQL(sql);
        sql = "INSERT INTO product VALUES (null, 'Quan Ao', 500000)";
        db.execSQL(sql);
        sql = "INSERT INTO product VALUES (null, 'Laptop', 1200000)";
        db.execSQL(sql);
    }

    //Phương thức này tự động gọi khi đã có DB trên Storage, nhưng phiên bản khác
    //với DATABASE_VERSION
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Xóa bảng cũ
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
        //Tạo bảng mới
        onCreate(db);
    }
}

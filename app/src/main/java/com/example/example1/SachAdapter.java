package com.example.example1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class SachAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Sach> sachList;

    public SachAdapter(Context context, int layout, List<Sach> sachList) {
        this.context = context;
        this.layout = layout;
        this.sachList = sachList;
    }

    @Override
    public int getCount() {
        return sachList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout,null);

        // Anh xa view
        TextView txtTieuDe = (TextView) view.findViewById(R.id.textViewTieuDe);
        TextView txtTacGia = (TextView) view.findViewById(R.id.textViewTacGia);
        TextView txtSoTrang = (TextView) view.findViewById(R.id.textViewSoTrang);

        // Gan Gia Tri
        Sach sach = sachList.get(i);

        txtTieuDe.setText(sach.getTieuDe());
        txtTacGia.setText(sach.getTacGia());
        txtSoTrang.setText(sach.getSoTrang());
        return view;
    }
}
